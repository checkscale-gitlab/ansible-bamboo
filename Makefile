.ONESHELL:

SHELL = /bin/sh

HEADER = "*****"

.PHONY: dev-create
dev-create: ## Starts and provisions the Vagrant environment
	@vagrant up

.PHONY: dev-update
dev-update: ## Updates Vagrant box
	@vagrant box update

.PHONY: dev-provision
dev-provision: ## Converge to desired state with Ansible
	@vagrant provision

.PHONY: dev-converge
dev-converge: ## Converge to desired state with Ansible
	@vagrant ssh -c 'cd /tmp/vagrant && chmod +x converge.sh && ./converge.sh'

.PHONY: dev-ssh
dev-ssh: ## SSH the Vagrant VM
	@vagrant ssh

.PHONY: dev-stop
dev-stop: ## Stops the Vagrant machine
	@vagrant halt

.PHONY: dev-restart
dev-restart: dev-stop dev-up

.PHONY: dev-status
dev-status: ## Outputs status of the Vagrant machine
	@vagrant status

.PHONY: dev-destroy
dev-destroy: ## Stops and deletes all traces of the Vagrant machine
	@vagrant destroy --force

.PHONY: dev-rebuild
dev-rebuild: dev-destroy dev-create dev-converge ## Destroys, creates and converges Vagrant VM

.PHONY: provision
provision:
	chmod +x provision.sh
	./provision.sh

.PHONY: converge
converge:
	chmod +x converge.sh
	./converge.sh

help: ## Show all commands available and their description
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
